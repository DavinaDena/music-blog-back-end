import Joi from "joi";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Comments } from "./Comments";
import { User } from "./User";

@Entity()

export class Topic{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column({type:'text'})
    text: string;

    @CreateDateColumn({default: ''})
    date: Date;

    @ManyToOne(() => User, user=> user.topic)
    user: User;
    
    @OneToMany(()=> Comments, comment => comment.topic)
    comment: Comments[]

    toJSON(){
        
        return {
            
            ...this,date:this.date.toLocaleDateString('fr-FR')
            //...this.date.toLocaleDateString('fr-FR')
            
        }
    }
}


export function TopicValidator(data: Object) {
    let schema = Joi.object({
        title: Joi.string().required(),
        text: Joi.string().required(),
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);
    if (error) {
        return error;
    } else {
        return true
    }
};
