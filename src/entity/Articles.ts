import Joi from "joi";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Category } from "./Category";
import { User } from "./User";
import { Genre } from "./Genre";

@Entity()

export class Articles {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    title: string;

    @Column()
    titleDescription: string;

    @Column({ type: "text" })
    articleText: string;

    @CreateDateColumn({ default: '' })
    date: Date;

    @ManyToOne(() => User, user => user.article)
    user: User

    @ManyToOne(() => Category, category => category.article)
    category: Category

    @ManyToOne(() => Genre, genre => genre.article)
    genre: Genre

    toJSON() {
        return {
            ...this, date: this.date.toLocaleDateString('fr-FR')
        }
    }
}

export function ArticleValidator(data: Object) {
    let schema = Joi.object({
        title: Joi.string().required(),
        titleDescription: Joi.string().required(),
        articleText: Joi.string().required(),

    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);
    if (error) {
        return error;
    } else {
        return true
    }
};
