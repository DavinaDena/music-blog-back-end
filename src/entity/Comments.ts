import Joi from "joi";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Articles } from "./Articles";
import { Topic } from "./Topic";
import { User } from "./User";

@Entity()

export class Comments{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type:'text'})
    text: string;

    @CreateDateColumn({default:''})
    date: Date;

    @ManyToOne(()=> Topic, topic => topic.comment)
    topic: Topic

    @ManyToOne (()=> User, user => user.comment)
    user: User

    @ManyToOne (()=> Articles, article => article.comment)
    article: Articles

    toJSON(){
        //console.log("coucou");
        
        return {
            
            ...this,date:this.date.toLocaleDateString('fr-FR')
            //...this.date.toLocaleDateString('fr-FR')
            
        }
    }
}


export function CommentsValidator(data: Object) {
    let schema = Joi.object({
        text: Joi.string().required(),
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);
    if (error) {
        return error;
    } else {
        return true
    }
};
