import Joi from "joi";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Articles } from "./Articles";

@Entity()

export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: '' })
    userName: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({ default: 18 })
    age: number;

    @Column({ default: 'user' })
    role: string


    @OneToMany(() => Articles, article => article.user)
    article: Articles[]

}

export function UserValidator(data: Object) {
    let schema = Joi.object({
        userName: Joi.string().min(2).max(30).allow(''),
        firstName: Joi.string().min(3).max(20).required(),
        lastName: Joi.string().min(3).max(20).required(),
        email: Joi.string().email().required(),
        password: Joi.string().required(),
        age: Joi.number().allow(''),
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);
    if (error) {
        return error;
    } else {
        return true
    }
};

