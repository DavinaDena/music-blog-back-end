import Joi from "joi";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Articles } from "./Articles";


@Entity()

export class Genre {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    genre: string;

    @OneToMany(() => Articles, article => article.category)
    article: Articles[]
}

export function CategoryValidator(data: Object) {
    let schema = Joi.object({
        genre: Joi.string().required()
    })

    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, value } = schema.validate(data, options);
    if (error) {
        return error;
    } else {
        return true
    }
};
