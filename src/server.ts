import express from 'express';
import cors from 'cors';
import { configurePassport } from '../utils/token';
import passport from 'passport';
import { userRouter } from './controller/userController';
import { articlesRouter } from './controller/articlesController';
import { categoryRouter } from './controller/categoryController';
import { genreRouter } from './controller/genreController';

export const server = express();
configurePassport()
server.use(express.json())
server.use(cors());
server.use(passport.initialize())
server.use(express.static('public'))


server.use('/api/user', userRouter)
server.use('/api/article', articlesRouter)
server.use('/api/category', categoryRouter)
server.use('/api/genre', genreRouter)






