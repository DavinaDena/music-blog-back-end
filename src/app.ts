import 'dotenv-flow/config';
import { createConnection } from 'typeorm';
import { generateToken } from '../utils/token';
import { server } from './server';

const port = process.env.PORT || 8000;

createConnection({
    type: 'mysql',
    url: process.env.DATABASE_URL,
    synchronize: true,
    entities: ['src/entity/*.ts'],
});

server.listen(port, () => {
    console.log('listening on ' + port);
});

// Créer un token quasi infini
// so that we can make tests

//fake user for tests
console.log(generateToken({
    email: 'dani111@gmail.com',
    password: 'password123',
    id: 5,
    role: 'admin'
}));


