import { Router } from "express";
import { getRepository } from "typeorm";
import { User, UserValidator } from "../entity/User";
import bcrypt from 'bcrypt'
import { generateRefreshToken, generateToken } from "../../utils/token";
import passport from "passport";
import jwt from 'jsonwebtoken';


export const userRouter = Router()

//get all users to test, will be deleted
userRouter.get('/', async (req, res) => {
    try {
        let user = await getRepository(User).find()

        if (!user) {
            res.status(404).end()
            return
        }
        res.json(user)

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
//will be changed in the future 
userRouter.patch('/:id', async (req, res) => {
    try {

        let value = UserValidator(req.body)
        if (value != true) {
            return res.status(422).json(value.details[0].message)
        } else {
            const repo = getRepository(User)
            const user = await repo.findOne(req.params.id)

            if (!user) {
                res.status(404).end()
                return

            }
            Object.assign(user, req.body)
            repo.save(user)
            res.json(user)
        }

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

userRouter.post('/refreshToken', (req, res) => {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if (token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.REFRESH_TOKEN_SECRET, async (err, user) => {
        if (err) {
            return res.sendStatus(401)
        }

        const exists = await getRepository(User).findOne({ where: [{ id: user.id }] })

        if (exists) {
            delete user.iat
            delete user.exp
            const refreshedAccessToken = generateToken({
                email: exists.email,
                userName: exists.userName,
                id: exists.id,
                role: exists.role

            })

            const newRefreshToken = Date.now() / 1000 > user.exp - 10000000 ? generateRefreshToken({
                id: exists.id
            }) : undefined

            return res.json({
                token: refreshedAccessToken,
                refresh_token: newRefreshToken ?? null

            })
        }
        return res.status(401).json({ error: 'Invalid token' });
    })
})


//check account as an admin
userRouter.get('/account', passport.authenticate('jwt', { session: false }), async (req, res) => {
    let user = await getRepository(User).findOne(req.user.id, ({ relations: ['comment'] }))
    res.json(user)
})



//user Login
userRouter.post('/login', async (req, res) => {
    try {
        const user = await getRepository(User).findOne({ email: req.body.email })
        if (user) {

            let samePWD = await bcrypt.compare(req.body.password, user.password)

            if (samePWD) {
                return res.json({
                    user,
                    token: generateToken({
                        id: user.id,
                        email: user.email,
                        userName: user.userName,
                        role: user.role
                    }),
                    refresh_token: generateRefreshToken({
                        id: user.id
                    })
                });
            }
            res.status(401).json({ error: 'Incorrect password' })
            return
        }
        res.status(404).json({ error: 'Sorry, seems we couldnt find you in our database' })


    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


//user Register
userRouter.post('/register', async (req, res) => {

    try {
        let val = UserValidator(req.body)

        if (val != true) {
            return res.status(422).json({ error: val.details[0].message })

        } else {

            const newUser = new User;
            Object.assign(newUser, req.body);
            const exists = await getRepository(User).findOne({ email: newUser.email })

            if (exists) {
                res.status(409).json({ error: 'Email already exists' })
                return
            }

            newUser.role = 'user'
            newUser.password = await bcrypt.hash(newUser.password, 11)

            await getRepository(User).save(newUser)
            res.status(201).json({
                user: newUser,
                token: generateToken({
                    email: newUser.email,
                    userName: newUser.userName,
                    id: newUser.id,
                    role: newUser.role
                }),
                refresh_token: generateRefreshToken({
                    id: newUser.id
                })
            })
        }

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

