import { Router } from "express";
import { getRepository } from "typeorm";
import { Category } from "../entity/Category";



export const categoryRouter = Router()

//GET CATEGORY BY ID
categoryRouter.get('/:id', async (req, res) => {
    try {

        const category = await getRepository(Category).findOne(req.params.id, ({ relations: ['article'] }))

        if (!category) {
            res.status(404).end()
            return
        }

        res.json(category)

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//GET ALL CATEGORIES
categoryRouter.get('/', async (req, res) => {
    try {
        let category = await getRepository(Category).find()

        if (!category) {
            res.status(404).end()
            return
        }
        res.json(category)


    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})



//TO POST A NEW CATEGORY
categoryRouter.post('/', async (req, res) => {
    try {

        let category = new Category()
        Object.assign(category, req.body)


        await getRepository(Category).save(category)
        return res.status(201).json(category)

    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
})