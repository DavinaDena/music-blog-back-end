import { Router } from "express";
import { exist } from "joi";
import passport from "passport";
import { getRepository } from "typeorm";
import { Comments, CommentsValidator } from "../entity/Comments";
import { Topic, TopicValidator } from "../entity/Topic";



export const topicsRouter = Router()

//get topic by id
topicsRouter.get('/:id', async (req, res) => {
    try {
        const topic = await getRepository(Topic).findOne(req.params.id, ({ relations: ['comment', 'user', 'comment.user'] }))
        if (!topic) {
            res.status(404).end()
            return
        }
        res.json(topic)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


//get all topics
topicsRouter.get('/', async (req, res) => {
    try {
        const topic = await getRepository(Topic).find({ order: { id: "DESC" }, relations: ['user'] })
        if (!topic) {
            res.status(404).end()
            return
        }
        res.json(topic)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


//post a topic
topicsRouter.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let val = TopicValidator(req.body)
        if (val != true) {
            return res.status(422).json(val.details[0].message)
        } else {

            let topic = new Topic()
            Object.assign(topic, req.body);
            topic.user = req.user

            await getRepository(Topic).save(topic)
            return res.status(201).json(topic)
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }

})
//req.user.role === "admin" || 
//delete topic
topicsRouter.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const repo = getRepository(Topic)

        const topic = await repo.findOne(req.params.id, ({ relations: ['user'] }))
        if (topic.user.id == req.user.id) {

            const result = await repo.delete(req.params.id)

            if (result.affected !== 1) {
                res.status(404).end()
                return
            }
            res.status(204).end()
        }

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//modify an topic
topicsRouter.patch('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    //if (req.user.role == 'admin') {
    try {
        let val = TopicValidator(req.body)
        if (val != true) {
            return res.status(422).json(val.details[0].message)
        } else {
            const repo = getRepository(Topic)
            const topic = await repo.findOne(req.params.id)

            if (!topic) {
                res.status(404).end()
                return
            }
            Object.assign(topic, req.body)
            repo.save(topic)
            res.json(topic)
        }
    } catch (error) {
    }

    //} else {
    //     res.status(403).json({ error: "Vous n'avez pas les droits nécessaires" });
    // }

})

//TO POST A COMMENT ON A TOPIC/FORUM
topicsRouter.post('/:id/comment', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        let val = CommentsValidator(req.body)
        if (val != true) {
            return res.status(422).json(val.details[0].message)
        } else {

            const repo = getRepository(Topic)
            const topic = await repo.findOne(req.params.id)

            if (!topic) {
                res.status(404).end()
                return
            }

            let comment = new Comments()
            Object.assign(comment, req.body)
            comment.user = req.user
            comment.topic = topic

            await getRepository(Comments).save(comment)
            res.status(201).json(comment)

        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


//TO DELETE A COMMENT

topicsRouter.delete('/comment/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {

        const exists = await getRepository(Comments).findOne(req.params.id, ({ relations: ['user'] }))
        
        
        if (exists) {
            if (req.user.id === exists.user.id) {
                await getRepository(Comments).delete(req.params.id)
                return res.status(204).end()

            } return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })

        } return res.status(404).end()



        // const repo = getRepository(Comments)
        // const result = await repo.delete(req.params.id)
        // if (result.affected !== 1) {
        //     res.status(404).end()
        //     return
        // }
        // res.status(204).end()



    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})