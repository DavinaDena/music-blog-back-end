import { Router } from "express";
import passport from "passport";
import { getRepository } from "typeorm";
import { Articles, ArticleValidator } from "../entity/Articles";


export const articlesRouter = Router()

//get all articles

articlesRouter.get('/', async (req, res) => {
    try {
        let article = await getRepository(Articles).find({ order: { id: "DESC" } })


        if (!article) {
            res.status(404).end()
            return
        }
        res.json(article)

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//get articles by id
articlesRouter.get('/:id', async (req, res) => {
    try {

        const article = await getRepository(Articles).findOne(req.params.id, ({ relations: ['category'] }))

        if (!article) {
            res.status(404).end()
            return
        }

        res.json(article)

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//Add an article
//Only Admin can
articlesRouter.post('/',passport.authenticate('jwt',{session:false}), async (req, res) => {
    try {
        let val = ArticleValidator(req.body)
        if (val != true) {
            return res.status(422).json(val.details[0].message)
        } else {

            let article = new Articles()
            Object.assign(article, req.body)
            article.user = req.user

            await getRepository(Articles).save(article)
            return res.status(201).json(article)


        }

    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }

})

//delete an article
articlesRouter.delete('/:id',passport.authenticate('jwt',{session:false}),    async (req, res) => {
        if (req.user.role == 'admin') {
        try {
            const repo = getRepository(Articles)
            const result = await repo.delete(req.params.id)
            if (result.affected !== 1) {
                res.status(404).end()
                return
            }
            res.status(204).end()

        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }

        } else {
            res.status(403).json({ error: "Vous n'avez pas les droits nécessaires" });
        }
    })

//modify an article
articlesRouter.patch('/:id' ,passport.authenticate('jwt',{session:false}), async (req, res) => {

      if (req.user.role == 'admin') {
        try {
            let val = ArticleValidator(req.body)
            if (val != true) {
                return res.status(422).json(val.details[0].message)
            } else {
                const repo = getRepository(Articles)
                const article = await repo.findOne(req.params.id)

                if (!article) {
                    res.status(404).end()
                    return
                }
                Object.assign(article, req.body)
                repo.save(article)
                res.json(article)

            }
        } catch (error) {
            console.log(error);
            res.status(500).json(error);
        }

        } else {
            res.status(403).json({ error: "Vous n'avez pas les droits nécessaires" });
        };
    })


//TO POST A COMENT ON AN ARTICLE (later on)
// articlesRouter.post('/:id/comment', passport.authenticate('jwt', { session: false }), async (req, res) => {
//     try {
//         let val = CommentsValidator(req.body)
//         if(val!= true){
//             return res.status(422).json(val.details[0].message)
//         }else{
//             const repo = getRepository(Articles)
//             const article = await repo.findOne(req.params.id)

//             if(!article){
//                 res.status(404).end()
//                 return
//             }

//             let comment = new Comments()
//             Object.assign(comment, req.body)
//             comment.user = req.user
//             comment.article= article

//             await getRepository(Comments).save(comment)
//             res.status(201).json(comment)
//         }
//     } catch (error) {
//         console.log(error);
//         res.status(500).json(error);
//     }
// })


// articlesRouter.delete('/comment/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
//     try {

//         const exists = await getRepository(Comments).findOne(req.params.id, ({ relations: ['user'] }))
        
        
//         if (exists) {
//             if (req.user.id === exists.user.id) {
//                 await getRepository(Comments).delete(req.params.id)
//                 return res.status(204).end()

//             } return res.status(401).json({ message: "vous n'avez pas les droits nécessaires" })

//         } return res.status(404).end()



//         // const repo = getRepository(Comments)
//         // const result = await repo.delete(req.params.id)
//         // if (result.affected !== 1) {
//         //     res.status(404).end()
//         //     return
//         // }
//         // res.status(204).end()



//     } catch (error) {
//         console.log(error);
//         res.status(500).json(error);
//     }
// })

