import { Router } from "express";
import { getRepository } from "typeorm";
import { Genre } from "../entity/Genre";


export const genreRouter = Router()


genreRouter.get('/:id', async (req, res) => {
    try {

        const category = await getRepository(Genre).findOne(req.params.id, ({ relations: ['article'] }))

        if (!category) {
            res.status(404).end()
            return
        }

        res.json(category)

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

genreRouter.get('/', async (req, res) => {
    try {
        let category = await getRepository(Genre).find()

        if (!category) {
            res.status(404).end()
            return
        }
        res.json(category)


    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})



//TO POST A NEW CATEGORY
genreRouter.post('/', async (req, res) => {
    try {

        let category = new Genre()
        Object.assign(category, req.body)


        await getRepository(Genre).save(category)
        return res.status(201).json(category)

    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
})